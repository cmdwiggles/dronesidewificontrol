#!/usr/bin/python
###################################
#
#	Class for alloting wifi radios to RX & TX classes
#
##################################
import threading
import subprocess
import time
import os

WBC_PATH_TX="/home/pi/wifibroadcast/tx"
WBC_PATH_RX="/home/pi/wifibroadcast/rx"

class radiocntrl:
	############################################
	#		Init function sets up class variables and detects what wifi radios are connected
	#		Counts for available radios is set to all available
	############################################
	def __init__(self):
		
		# These variables should be used from the outside
		self.dev_list = {'slow_tx':[],'fast_tx':[]}
		self.radios_in_use = []
		self.available_tx = 0
		self.available_rx = 0
		self.active_ps = []
		self.tx_pipes = []
		self.tx_monitors = []

		# lshw linux command is required for detecting the hardware
		self.lshw_out = subprocess.check_output(['lshw','-c','Network'])
		self.port_arr = self.lshw_out.split('*-network:')
		self.port_arr.pop(0)
		subprocess.call(['service','ifplugd','stop'])
		# Parse through the network devices to extract wifi radios
		for self.port in self.port_arr:
			self.details_str = self.port.split('\n')
			self.dev_dict = {}
			for self.idx in range(len(self.details_str)):
				if  self.details_str[self.idx].strip() is '':
					continue
				else:
					self.tmp_arr = self.details_str[self.idx].strip().split(': ')
					if len(self.tmp_arr) == 2:
						self.dev_dict[self.tmp_arr[0]]= self.tmp_arr[1]
			if 'description' in self.dev_dict.keys():
				if self.dev_dict['description'] == 'Wireless interface':
					if 'logical name' in self.dev_dict and 'configuration' in self.dev_dict:
						if 'driver=ath9k_htc' in self.dev_dict['configuration']:
							self.dev_list['fast_tx'].append(self.dev_dict['logical name'].strip())
							self.available_tx += 1
						else:
							self.dev_list['slow_tx'].append(self.dev_dict['logical name'].strip())
							self.available_rx += 1

	# Function to start a wifi radio in TX mode and init tx pipes with given params
	def start_tx(self,channel=13,fec_packets=4,packet_len=1450,port_start=0,blk_len=8,retran=1,min_bytes=0,streams=2):

		if self.available_tx > 0:
			if self._prep_radio(self.dev_list['fast_tx'][-1],channel):
				if streams <2:
					streams = 2

				self.active_ps.append(subprocess.Popen([WBC_PATH_TX,'-r',str(fec_packets),\
					'-f',str(packet_len),'-p',str(port_start),'-b',str(blk_len),\
					'-x',str(retran),'-m',str(min_bytes),self.dev_list['fast_tx'][-1]],\
					stdout=subprocess.PIPE))

				self.tx_monitors.append(_tx_ps_monitor(self.active_ps[-1].stdout))

				for stream_idx in range(port_start,(port_start + streams)):
					pipe_path = "/tmp/fifo"+str(stream_idx)
					subprocess.call(['touch',pipe_path])
					subprocess.call(['chmod','777',pipe_path])
					self.tx_pipes.append({'pipe':(open(pipe_path,'w')),'in_use':False,\
						'usr_id':'','radio_id':self.dev_list['fast_tx'][-1]})

				self.radios_in_use.append(self.dev_list['fast_tx'].pop())
				self.available_tx += -1

				return True
			else:
				return False
		else:
			return False


	def _prep_radio(self,NIC,CHANNEL):
		subprocess.call(['ifconfig',NIC,'down'])
		subprocess.call(['iw','dev',NIC,'set','monitor','otherbss','fcsfail'])
		subprocess.call(['ifconfig',NIC,'up'])
		subprocess.call(['iwconfig',NIC,'channel',str(CHANNEL)])
		return True

	#	This function doesnt quite work yet
	def _release_radio(self,NIC):
		subprocess.call(['ifconfig',NIC,'down'])
		subprocess.call(['iwconfig',NIC,'mode','managed'])
		subprocess.call(['ifconfig',NIC,'up'])


	def __del__(self):
		for monitor in self.tx_monitors:
			monitor.stop_mon()
		for radio_id in self.radios_in_use:
			self._release_radio(radio_id)

		for ps in self.active_ps:
			ps.kill()
		print(self.tx_pipes)
		for pipe in self.tx_pipes:
			pipe['pipe'].close()


class _tx_ps_monitor(threading.Thread):
	def __init__(self,tx_ps_stdout):
		self.tx_ps_out = tx_ps_stdout
		self.curr_line = ""
		
		self.read_thread = threading.Thread(target=self._tx_read_thread)
		

	def _tx_read_thread(self):
		while self.monitor:
			buff = self.tx_ps_out.readline()
			print("test")
			if buff.strip():
				self.curr_line = buff.strip()
				print(buff)
				print("buff_test")
			else:
				time.sleep(0.5)

	def start_mon(self):
		if not self.monitor:
			self.monitor = True
			self.read_thread.start()


	def stop_mon(self):
		self.monitor = False
		self.read_thread.join()

	def __del__(self):
		self.stop_mon()

			





############################################################



if __name__ == '__main__':
	testclass = radiocntrl()
	print(testclass.dev_list)
	testclass.start_tx(channel=1)
	print("supertest")
