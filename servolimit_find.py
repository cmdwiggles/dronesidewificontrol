#!/usr/bin/env python

from RPIO import PWM
import time

throttle_pin = 27
aileron_pin = 	17
elevator_pin = 18
rudder_pin = 22
aux1_pin = 23
delay = 1.5

servo_long = 2000
servo_short = 1200


throttle_full = 2350	#157 fulle
throttle_cut = 1150		#idle	0

aileron_left = 850		# -171
aileron_mid = 1500
aileron_right = 2100	# 146 right

elevator_forward = 850	# 171	forward
elevator_mid	= 1500
elevator_back	= 2100	# 148 back

rudder_left = 2100	# 146	right
rudder_mid = 1500
rudder_right  = 850	# -171	right

aux1_on = 2100	# 146	on
aux1_off = 850	# 171	off

# throttle_full = servo_long	#157 fulle
# throttle_cut = servo_short		#idle	0

# aileron_left = servo_short		# -160
# aileron_right = servo_long	# 140 right

# elevator_forward = servo_short	# 160	forward
# elevator_back	= servo_long	# 140 back

# rudder_left = servo_long	# 140	right
# rudder_right  = servo_short	# -160	right

# aux1_on = servo_long	# 140	on
# aux1_off = servo_short	# 160	off

servo = PWM.Servo()
PWM.set_loglevel(1)

for x in range(0, 100):
	print("throttle_pin %d" % throttle_full)
	servo.set_servo(throttle_pin, throttle_full)
	time.sleep(delay)

	print("throttle_pin %d" % throttle_cut)
	servo.set_servo(throttle_pin, throttle_cut)
	time.sleep(delay)


	print("aileron_pin %d" % aileron_left)
	servo.set_servo(aileron_pin, aileron_left)
	time.sleep(delay)

	print("aileron_pin %d" % aileron_right)
	servo.set_servo(aileron_pin, aileron_right)
	time.sleep(delay)

	print("aileron_pin %d" % aileron_mid)
	servo.set_servo(aileron_pin, aileron_mid)
	time.sleep(delay)



	print("elevator_pin %d" % elevator_forward)
	servo.set_servo(elevator_pin, elevator_forward)
	time.sleep(delay)

	print("elevator_pin %d" % elevator_back)
	servo.set_servo(elevator_pin, elevator_back)
	time.sleep(delay)

	print("elevator_pin %d" % elevator_mid)
	servo.set_servo(elevator_pin, elevator_mid)
	time.sleep(delay)


	print("rudder_pin %d" % rudder_left)
	servo.set_servo(rudder_pin, rudder_left)
	time.sleep(delay)

	print("rudder_pin %d" % rudder_right)
	servo.set_servo(rudder_pin, rudder_right)
	time.sleep(delay)

	print("rudder_pin %d" % rudder_mid)
	servo.set_servo(rudder_pin, rudder_mid)
	time.sleep(delay)



	print("aux1_pin %d" % aux1_on)
	servo.set_servo(aux1_pin, aux1_on)
	time.sleep(delay)

	print("aux1_pin %d" % aux1_off)
	servo.set_servo(aux1_pin, aux1_off)
	time.sleep(delay)



servo.stop_servo(throttle_pin)
servo.stop_servo(aileron_pin)
servo.stop_servo(elevator_pin)
servo.stop_servo(rudder_pin)
servo.stop_servo(aux1_pin)