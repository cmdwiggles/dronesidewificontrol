#!/usr/bin/env python2
import time
import os

pi_blaster_fifo = "/dev/pi-blaster"

################################
throttle_pin = 27
aileron_pin = 17
elevator_pin = 18
rudder_pin = 22
aux1_pin = 23

fr_serv = 100
srv_neutral =0.15
aileron_div = 11000.0
elevator_div = 11000.0
rudder_div = -11000.0
throttle_div = 64.0
aux1_div = 0.33

throttle_div = 64.0
throttle_div_pos = 64.0
throttle_div_neg = 128.0

aileron_neutral = 7.3
elevator_eutral = 6.9	#7.15
rudder_neutral = 7.15
throttle_neutral = 6.0
aux1_neutral = 5
################################

pins = {'throttle':27,\
		'aileron':17,\
		'elevator':18,\
		'rudder':22,\
		'aux1':23}

################################
pwm_pipe = open(pi_blaster_fifo,'w',0)

################################

while True:
	for pin in pins:
		#os.system('echo "%d=%f" > /dev/pi-blaster' % (pins[pin], 0.1))
		pwm_str = "%d=%f\n" % (pins[pin], 0.1)
		pwm_pipe.write(pwm_str)
	time.sleep(1)

	for pin in pins:
		#os.system('echo "%d=%f" > /dev/pi-blaster' % (pins[pin], 0.15))
		pwm_str = "%d=%f\n" % (pins[pin], 0.15)
		pwm_pipe.write(pwm_str)
	time.sleep(1)

	for pin in pins:
		#os.system('echo "%d=%f" > /dev/pi-blaster' % (pins[pin], 0.2))
		pwm_str = "%d=%f\n" % (pins[pin], 0.2)
		pwm_pipe.write(pwm_str)
	time.sleep(1)

