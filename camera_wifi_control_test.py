#!/usr/bin/env python2.7
import subprocess
import os
import sys
import time
import RPi.GPIO as GPIO

WIDTH = 1280
HEIGHT = 720
FPS = 48
BITRATE = 4000000
KEYFRAMERATE = 48
ROTATION = 180

###############################
#
#	wlan0	RT2870/RT3070
#	wlan1	AR9271
#
###############################
CONTROL_BLOCK_SIZE=1			# -b
CONTROL_FECS=0					# -r
CONTROL_PACKET_LENGTH=512		# -f
CONTROL_PORT=1					# -p


CONTROL_CHANNEL = 1
CONTROL_NICS="wlan0"

###############################
VID_BLOCK_SIZE=8
VID_FECS=4
VID_PACKET_LENGTH=1024
VID_PORT=0

VID_CHANNEL = 13
VID_NICS="wlan1"



################################
WBC_PATH_TX="/home/pi/wifibroadcast/tx"
WBC_PATH_RX="/home/pi/wifibroadcast/rx"
################################

XBOX_PWM_PATH = "/home/pi/drone1/xbox_pwm_control.py"

throttle_pin = 27
aileron_pin = 17
elevator_pin = 18
rudder_pin = 22
aux1_pin = 23
delay = 0.5

left = 1000
middle = 1500
right = 2000





#################################
def getCPUtemperature():
    res = os.popen('vcgencmd measure_temp').readline()
    return(res.replace("temp=","").replace("'C\n",""))

def prep_radio(NIC,CHANNEL):
	subprocess.call(['ifconfig',NIC,'down'])
	subprocess.call(['iw','dev',NIC,'set','monitor','otherbss','fcsfail'])
	subprocess.call(['ifconfig',NIC,'up'])
	subprocess.call(['iwconfig',NIC,'channel',str(CHANNEL)])


################################

if os.getuid() != 0:
    print("Not sudo user,\nExiting")
    sys.exit()

print("Core temperature:\t" +getCPUtemperature())
print(sys.version)
start = time.time()
print("python wifi control starting")


################################
skip = False
if not skip:
	print("python wifi control setup starting")

	for nic in CONTROL_NICS.split():
		print("Setting control card: %s to channel %d\t\t" % (nic, CONTROL_CHANNEL)),
		prep_radio(nic,CONTROL_CHANNEL)
		print("Success")

	for nic in CONTROL_NICS.split():
		print("Setting video card: %s to channel %d\t\t" % (VID_NICS, VID_CHANNEL)),
		prep_radio(VID_NICS,VID_CHANNEL)
		print("Success")

################################

#try:
camera_tx_ps = subprocess.Popen(['raspivid','-ih','-t','0',\
	'-w',str(WIDTH),'--height',str(HEIGHT),\
	'-fps',str(FPS),'-rot',str(ROTATION),'-b',str(BITRATE),\
	'-n','-g',str(KEYFRAMERATE),'-pf','high','-o','-'],\
	stdout=subprocess.PIPE)

video_tx_ps = subprocess.Popen([WBC_PATH_TX,'-b',str(VID_BLOCK_SIZE),\
	'-r',str(VID_FECS),'-f',str(VID_PACKET_LENGTH),VID_NICS,"&"],\
	stdin=camera_tx_ps.stdout, stdout=subprocess.PIPE)


control_rx_ps = subprocess.Popen([WBC_PATH_RX,'-p',str(CONTROL_PORT),'-b',str(CONTROL_BLOCK_SIZE),\
	'-r',str(CONTROL_FECS),'-f',str(CONTROL_PACKET_LENGTH),CONTROL_NICS], stdout=subprocess.PIPE)

servo_pwm = subprocess.Popen(['python',XBOX_PWM_PATH], stdin=control_rx_ps.stdout)



while True:
	# line = control_rx_ps.stdout.readline()
	# if line:
	# 	print(line)
	# 	line_arr = line.split(',')
	# 	if len(line_arr) == 5:
	# 		print(line)

	# print(line)
	time.sleep(0.1)

# except:
# 	camera_tx_ps.kill()
# 	video_tx_ps.kill()
# 	control_rx_ps.kill()



time.sleep(5)
end =time.time()
print((end - start) * 1000)



#############################
print("Killing sub processes")
camera_tx_ps.kill()
video_tx_ps.kill()
control_rx_ps.kill()

#############################