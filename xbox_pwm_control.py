import os
import sys
import time
import sys
################################

last_time = time.time()
timeout = 10.0
throttle_idling = 0.105

################################
throttle_pin = 27
aileron_pin = 17
elevator_pin = 18
rudder_pin = 22
aux1_pin = 23

fr_serv = 100
srv_neutral =0.149
aileron_div = 655360.0
elevator_div = -655360.0
rudder_div = -655360.0
throttle_div = 2560.0
aux1_div = 10.0

throttle_div = 64.0
throttle_div_pos = 2300.0
throttle_div_neg = 12800.0

aileron_neutral = srv_neutral
elevator_eutral = srv_neutral
rudder_neutral = srv_neutral
throttle_neutral = 0.1
aux1_neutral = 0.1
################################


controls = {'X1':0,\
		'Y1':0,\
		'X2':0,\
		'Y2':0,\
		'du':0,\
		'du':0,\
		'dd':0,\
		'dl':0,\
		'dr':0,\
		'back':0,\
		'guide':0,\
		'start':0,\
		'TL':0,\
		'TR':0,\
		'A':0,\
		'B':0,\
		'X':0,\
		'Y':0,\
		'LB':0,\
		'RB':0,\
		'LT':0,\
		'RT':0,
		'du_T':0,\
		'du_T':0,\
		'dd_T':0,\
		'dl_T':0,\
		'dr_T':0,\
		'back_T':0,\
		'guide_T':0,\
		'start_T':0,\
		'TL_T':0,\
		'TR_T':0,\
		'A_T':0,\
		'B_T':0,\
		'X_T':0,\
		'Y_T':0,\
		'LB_T':0,\
		'RB_T':0,\
		'idx':0,\
		'fresh':False\
		}
################################
pi_blaster_fifo = "/dev/pi-blaster"
pwm_pipe = open(pi_blaster_fifo,'w',0)

def pwm_write(pin,val):
	pwm_str = "%d=%f\n" % (pin,val)
	pwm_pipe.write(pwm_str)

################################
def convert_line(line_arr):
	global controls
	global last_time
	old_val = 0
	for ctrl in line_arr:
		try:
			(var,val) = ctrl.split(':')
			if var in controls.keys():
				#print(var)
				if var.strip() == 'idx':
					#print("%d\t%d"%(controls['idx'],int(val)))

					if int(val) == int(controls['idx']):
						controls['fresh'] = False
						#print("yes")
					else:
						old_val = controls['idx']
						controls['idx'] = int(val)
						controls['fresh'] = True
						print("yes")
						#print("%d %d %r"%(old_val,controls['idx'],controls['fresh']))
				else:
					controls[var] = int(val)
		except:
			print("Error parsing: %s" %(ctrl))
	last_time = time.time()


###############################


try:
	buff = ''
	pwm_write(throttle_pin,throttle_neutral)
	pwm_write(aileron_pin,aileron_neutral)
	pwm_write(elevator_pin,elevator_eutral)
	pwm_write(rudder_pin,rudder_neutral)
	pwm_write(aux1_pin,aux1_neutral)
	while True:
		if time.time() >= (last_time + timeout):
			pwm_write(throttle_pin,throttle_idling)
			pwm_write(aileron_pin,aileron_neutral)
			pwm_write(elevator_pin,elevator_eutral)
			pwm_write(rudder_pin,rudder_neutral)
			pwm_write(aux1_pin,aux1_neutral)

		buff = sys.stdin.readline()
		if buff:
			#print(buff)
			line_arr = buff.split(',')
			#print(len(line_arr))
			if len(line_arr) >= 36:
					convert_line(line_arr)
					#print(controls['idx'])
					#print(controls['fresh'])
					if controls['fresh']:
						aileron = (controls['X1'] / aileron_div) + aileron_neutral
						elevator = (controls['Y1'] / elevator_div)+ elevator_eutral
						rudder =(controls['X2'] / rudder_div)+ rudder_neutral
						throttle = (controls['RT'] / throttle_div_pos) + throttle_neutral
						aux1 = (controls['start_T'] / aux1_div) + aux1_neutral

						cntrl_str = "%.3f,%3f,%3f,%3f,%3f " % (aileron,elevator,rudder,throttle,aux1)
						#print(buff)
						#print(cntrl_str)
						pwm_write(throttle_pin,throttle)
						pwm_write(aileron_pin,aileron)
						pwm_write(elevator_pin,elevator)
						pwm_write(rudder_pin,rudder)
						pwm_write(aux1_pin,aux1)

except KeyboardInterrupt:
   sys.stdout.flush()
   pass

