#!/bin/bash
# tx script


#adapt these to your needs
NIC="wlan1"
CHANNEL="13"

VID_CHANNEL="13"
CONTROL_CHANNEL="1"

VID_NICS="wlan1"
CONTROL_NICS="wlan0"

WIDTH=1280
HEIGHT=720
FPS=48
BITRATE=4000000
KEYFRAMERATE=48

##################################

#change these only if you know what you are doing (and remember to change them on both sides)
BLOCK_SIZE=8
FECS=4
PACKET_LENGTH=1024
PORT=0

##################################

WBC_PATH="/home/pi/wifibroadcast"



function prepare_nic {
	echo "updating wifi ($1, $2)"
	ifconfig $1 down
	iw dev $1 set monitor otherbss fcsfail
	ifconfig $1 up
	iwconfig $1 channel $2
}

# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

for NIC in $VID_NICS
do
	prepare_nic $NIC $VID_CHANNEL
done

for NIC in $CONTROL_NICS
do
	prepare_nic $NIC $CONTROL_CHANNEL
done


echo "Starting tx for $NIC"

#echo "raspivid -ih -t 0 -w $WIDTH -h $HEIGHT -fps $FPS -b $BITRATE -n -g $KEYFRAMERATE -pf high -o - | $WBC_PATH/tx -p $PORT -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $VID_NICS"
#echo "raspivid -ih -t 0 -w 1280 -h 720 -fps 48 -b 4000000 -n -g 48 -pf high -o - | /home/pi/wifibroadcast/tx -p 0 -b 8 -r 4 -f 1024 wlan1"
#echo "$WBC_PATH/rx -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $NIC"
#echo "/home/pi/wifibroadcast/rx -b 8 -r 4 -f 1024 wlan0"


#raspivid -ih -t 0 -w $WIDTH -h $HEIGHT -fps $FPS -b $BITRATE -n -g $KEYFRAMERATE -pf high -o - | $WBC_PATH/tx -p $PORT -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $VID_NICS &
#$WBC_PATH/rx -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $NIC

sudo $WBC_PATH/tx  $WBC_PATH/tx -p $PORT -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH -s 2 $VID_NICS &
sudo chmod 777 /tmp/fifo*
raspivid -ih -t 0 -w 1280 -h 720 -fps 30 -b 4000000 -n -g 60 -pf high -o - > /tmp/fifo0 &
#cat /dev/random > /tmp/fifo1
cat /dev/null > /tmp/fifo1