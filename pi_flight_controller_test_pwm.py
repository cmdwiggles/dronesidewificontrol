#!/usr/bin/env python

from RPIO import PWM
import time

throttle_pin = 27
aileron_pin = 	17
elevator_pin = 18
rudder_pin = 22
aux1_pin = 23
delay = 0.5

left = 1000
middle = 1500
right = 2000
servo = PWM.Servo()
PWM.set_loglevel(1)

for x in range(0, 100):
	print("pins left %d" % left)
	servo.set_servo(throttle_pin, left)
	#time.sleep(delay)
	servo.set_servo(aileron_pin, left)
	#time.sleep(delay)
	servo.set_servo(elevator_pin, left)
	#time.sleep(delay)
	servo.set_servo(rudder_pin, left)
	#time.sleep(delay)
	servo.set_servo(aux1_pin, left)

	
	time.sleep(delay)

	print("pins middle %d" % middle)
	servo.set_servo(throttle_pin, middle)
	#time.sleep(delay)
	servo.set_servo(aileron_pin, middle)
	#time.sleep(delay)
	servo.set_servo(elevator_pin, middle)
	#time.sleep(delay)
	servo.set_servo(rudder_pin, middle)
	#time.sleep(delay)
	servo.set_servo(aux1_pin, middle)

	time.sleep(delay)

	print("pins right %d" % right)
	servo.set_servo(throttle_pin, right)
	#time.sleep(delay)
	servo.set_servo(aileron_pin, right)
	#time.sleep(delay)
	servo.set_servo(elevator_pin, right)
	#time.sleep(delay)
	servo.set_servo(rudder_pin, right)
	#time.sleep(delay)
	servo.set_servo(aux1_pin, right)


	time.sleep(delay)
servo.stop_servo(throttle_pin)